# Baaqmd

## Introduction / Введение

This project uses Selenium, JUnit, Maven and Apache HTTP for writing and executing tests in Java 
within the IntelliJ IDEA development environment. To get started with the project, follow the
instructions below.

Этот проект использует Selenium, JUnit, Maven и Apache HTTP для написания и выполнения тестов на языке Java
в среде разработки IntelliJ IDEA. Чтобы начать работу с проектом, следуйте инструкциям ниже.

## Requirements / Требования

1. Install / Установите [Java Development Kit (JDK)](https://www.oracle.com/java/technologies/javase-jdk14-downloads.html) версии 8 или выше.
2. Install / Установите [IntelliJ IDEA](https://www.jetbrains.com/idea/download/) (Community Edition или Ultimate Edition).
3. Install / Установите [Git](https://git-scm.com/downloads) для клонирования проекта из репозитория GitLab.
4. Install / Установите [Maven](https://maven.apache.org/) для управления зависимостями

## Running Tests / Запуск тестов


1. Open the project in IntelliJ IDEA.
2. Locate the test directory in the project tree (usually `src/test/java/org.al`).
3. Right-click on the test directory and choose `Run 'SiteMapLinksTests` to execute  tests within the project.
____________________________________________________

1. Откройте проект в IntelliJ IDEA.
2. В дереве проекта найдите каталог с тестами (`src/test/java/org.al`).
3. Щелкните правой кнопкой мыши на файле с тестами и выберите `Run 'SiteMapLinksTests'` для запуска тестов в проекте.

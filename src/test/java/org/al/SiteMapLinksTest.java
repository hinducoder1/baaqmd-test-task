package org.al;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.fail;

public class SiteMapLinksTest {

    public static final String LOGO_URL = "/~/media/dotgov/images/logos/logo-no-tagline-png.gif?h=76&la=en&w=343&hash=BD046B806B8BF507E38F1E2C79D4928F";
    public static final String SITE_MAP_URL = "https://www.baaqmd.gov/sitemap";
    public static final int OK_STATUS_CODE = 200;
    private final WebDriver driver = new ChromeDriver();
    private final WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10)); // Установите максимальное время ожидания в секундах new надо переделать чтобы из бефора брал


    @ParameterizedTest
    @MethodSource("provideRefsForCheckAvailability")
    public void paramTest(String input) throws IOException {

        int statusCode = getHttpStatusCode(input);
        Assertions.assertEquals(OK_STATUS_CODE, statusCode, "Status not 200");
        driver.get(input);
        String xpathExpression = String.format("//img[@src='%s']", LOGO_URL);

        try {
            WebElement img = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathExpression)));
            boolean imageLoaded = isImageLoaded(driver, img);
            Assertions.assertTrue(imageLoaded, "image is not loaded");
        } catch (TimeoutException exception) {
            fail("Image is not found");
        }
    }

    @AfterEach
    public void shutDown() {
        driver.close();
    }

    private static Stream<Arguments> provideRefsForCheckAvailability() {
        WebDriver driver = new ChromeDriver();
        driver.get(SITE_MAP_URL);
        WebElement sitemap = driver.findElement(By.className("sitemap"));
        List<WebElement> linksList = sitemap.findElements(By.tagName("a"));
        Set<String> linksSet = linksList.stream()
                .map(element -> element.getAttribute("href"))
                .collect(Collectors.toSet());
        driver.close();
        return linksSet.stream()
                .map(Arguments::of);
    }

    private static int getHttpStatusCode(String url) throws IOException {
        HttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        HttpResponse httpResponse = httpClient.execute(httpGet);
        return httpResponse.getStatusLine().getStatusCode();
    }

    private static boolean isImageLoaded(WebDriver driver, WebElement image) {
        Object result = ((JavascriptExecutor) driver).executeScript(
                "return arguments[0].complete && " +
                        "typeof arguments[0].naturalWidth != 'undefined' && " +
                        "arguments[0].naturalWidth > 0", image);

        return result instanceof Boolean && (Boolean) result;
    }
}

